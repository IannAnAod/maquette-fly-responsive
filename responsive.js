// Fonction qui affiche ou masque le texte "More info"

var _hidediv = null;
function showdiv(id) {
    if (_hidediv)
        _hidediv();
    var div = document.getElementById(id);
    div.style.display = 'block';
    _hidediv = function () { div.style.display = 'none'; };
}

//Fonction qui gère le slide
//https://www.w3schools.com/w3css/w3css_slideshow.asp

var slideIndex = 1; //1ere photo
showDivs(slideIndex);

function plusDivs(n) {   //Ajoute ou retranche une image lorsqu'on clique sur un bouton
    showDivs(slideIndex += n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("carousel-item");
    if (n > x.length) {   //Si le slide appelle un numéro >au nb d'image, reprend à l'image 1 
        slideIndex = 1
    }
    if (n < 1) {  // Si slide < au nb d'image, fait correspondre l'image demandée à l'image à afficher
        slideIndex = x.length
    };
    for (i = 0; i < x.length; i++) {  // Affiche l'image
        x[i].style.display = "none";            //Cache l'élément
    }
    x[slideIndex - 1].style.display = "block";    //Montre l'élément
}

// Fonction qui gère les boutons sous le slide

function currentSlide(n) {
  showDivs(slideIndex = n);
}


